# LIS 4368 Mobile Web Applications

## Jonathan Conley

### Assignment #5 Requirements:

*Two Parts*

1. Clone files from BitBucket. 
2. Start Coding to make this beautiful server-side server work. 
3. Extra credit: Have a beautiful client-side. 
3. Link to tomcat: [http://localhost:9999/lis4368/index.jsp

#### Deliverables:
1. Compile the following class and servlet files:
2. Use the following instructions:
        a. Lesson6 - Remote Repos: Initial push (w/o clone)
            http://www.qcitr.com/usefullinks.htm#lesson6
        b. Lesson 7 - Regular Expressions
            http://www.qcitr.com/usefullinks.htm#lesson7
3. After completing the above steps, provide me with read-only access to the repository.
https://confluence.atlassian.com/bitbucket/repository-privacy-permissions-and-more-
221449716.html

#### Assignment Screenshots:


*First screenshot of A4*:

![Screenshot of A5_1](img/1.png)

*Second screenshot of A4*:

![Screenshot of A5_2](img/2.png)

*Third screenshot of A4*:

![Screenshot of A5_3](img/3.png)

#### [localhost:9999/lis4368](http://localhost:9999/lis4368)