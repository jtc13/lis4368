# LIS 4368 Mobile Web Applications

## Jonathan Conley

### Assignment #3 Requirements:

*Three Parts*

1. Coursetitle, yourname, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Links to the following files:
	a. a3.mwb
	b. a3.sql

#### Deliverables:
1. Provide Bitbucket read-only access to lis4368 repo, include links to the repos you created in the above tutorials in README.md must also include links and screenshots as per above.)
2. Blackboard Links: lis4381 Bitbucket repo

#### Assignment Screenshots:


*Screenshot of A3.mwb*:

![Screenshot of A3.mwb](img/MySQL.png)

*Another screenshot of A3.mwb working*:

![Screenshot of A3.mwb](img/MySQL2.png)

##### [MWB Link](mwb/a3.mwb)

##### [SQL Link](mwb/A3.sql)