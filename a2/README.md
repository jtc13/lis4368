> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Jonathan Conley

### Assignment #2 Requirements:

*Sub-Heading:*

1. http://localhost:9999/hello(displaysdirectory,needsindex.html)
2. http://localhost:9999/hello/HelloHome.html
(Rename "HelloHome.html" to "index.html")
3. http://localhost:9999/hello/sayhello (invokes HelloServlet)
Note: /sayhello maps to HelloServlet.class (changed web.xml file)
4. http://localhost:9999/hello/querybook.html
5. http://localhost:9999/hello/sayhi(invokesAnotherHelloServlet)

#### README.md file should include the following items:

* Assessment links(as above)
* Screenshot of the query results from the following link (see screenshot below):

#### Assignment Screenshots:

#### [localhost:9999/hello](http://localhost:9999/hello)
![](img/hello.png)

#### [localhost:9999/hello/index.html](http://localhost:9999/hello/index.html)
![](img/index.png)

#### [localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)
![](img/sayhello.png)

#### [localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
![](img/querybook.png)

#### [localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi)
![](img/sayhi.png)

#### [localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
![](img/selected.png)

#### [localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)
![](img/results.png)