# LIS 4368 Mobile Web Applications

## Jonathan Conley

### Project #2 Requirements:

1. Compile the following class and servlet files:
2. Include server-side validation from A4.
3. README displays screenshots of pre-, post-valid user form entry, as well as MySQL
customer table entry (see screenshots below).
4. Use git to push *all* lis4368 files and changes to your remote course Bitbucket repo

#### Assignment Screenshots:


*First screenshot of P2*:

![Screenshot of A5_1](img/p21.png)

![Screenshot of P2_2](img/p22.png)

![Screenshot of P2_3](img/p23.png)

![Screenshot of P2_3](img/p24.png)

![Screenshot of P2_3](img/p25.png)