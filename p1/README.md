# LIS 4368 Web Application Development

## Jonathan Conley

### Project #1 Requirements:

*Three Parts*

1. Course title, your name, assignment requirements, as per A1;
2. Screenshots as per below examples;
3. Link to tomcat: [http://localhost:9999/lis4368/index.jsp](http://localhost:9999/lis4368/index.jsp "http://localhost:9999/lis4368/index.jsp")

#### Deliverables:
1. Provide Bitbucket read-only access to lis4368 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4368 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 


#### Assignment Screenshots:

*Screenshot of P1*:

![Screenshot of P1_1](img/p1_1.png)

*Another screenshot of P1*:

![Screenshot of P1_2](img/p1_2.png)

*Another screenshot of P1*:

![Screenshot of P1_3](img/p1_3.png)
