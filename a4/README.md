# LIS 4368 Mobile Web Applications

## Jonathan Conley

### Assignment #4 Requirements:

*Two Parts*

1. Clone files from BitBucket. 
2. Start Coding to make this beautiful server-side server work. 
3. Extra credit: Have a beautiful client-side. 
3. Link to tomcat: [http://localhost:9999/lis4368/index.jsp

#### Deliverables:
1. Provide Bitbucket read-only access to lis4368 repo, include links to the repos you created in the above tutorials in README.md must also include links and screenshots as per above.)
2. Blackboard Links: 4368 Bitbucket repo

#### Assignment Screenshots:


*First screenshot of A4*:

![Screenshot of A4_1](img/a4_1.png)

*Second screenshot of A4*:

![Screenshot of A4_2](img/a4_2.png)

#### [localhost:9999/lis4368](http://localhost:9999/lis4368)