> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS 4368  Mobile Web Application Development
## Jonathan Conley

###Course Work links

#### [A1 README.md](a1/README.md)
1. Screenshot of running java Hello (#1 above);
2. Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial); 3. git commands w/short descriptions;
4. Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes).

#### [A2 README.md](a2/README.md)
1. ProvideBitbucketread-onlyaccesstolis4368repo,includelinkstothereposyoucreatedinthe
above tutorials in README.md, using Markdown syntax
(README.md must also include screenshots as per above and below.)
(DO NOT create README in Bitbucket—ALWAYS do it locally, then push it to Bitbucket.)
2. BlackboardLinks:lis4368Bitbucketrepo

#### [A3 README.md](a3/README.md)
1. Coursetitle, yourname, assignment requirements, as per A1;
2. Screenshot of ERD;
3. Screenshot of running application’s first user interface;
4. Screenshot of running application’s second user interface;
5. Links to the following files:
	a. a3.mwb
	b. a3.sql

#### [P1 README.md](p1/README.md)
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4368 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 

#### [A4 README.md](a4/README.md)
1. Provide Bitbucket read-only access to lis4381 repo, include links to the other assignment repos you created in README.md, using Markdown syntax
2. Blackboard Links: lis4368 Bitbucket repo
3. *Note*: the carousel *must* contain (min. 3) slides that either contain text or images that link to other content areas marketing/promoting your skills. 

#### [A5 README.md](a5/README.md)
1. cd to webapps subdirectory:
Example (windows): cd C:/tomcat/webapps
2. Clone assignment starter files:
git clone https://bitbucket.org/mjowett/lis4368_student_files
(clones lis4368_student_files Bitbucket repo)
Note: If there is an existing lis4368_student_files, it *will* need to be renamed—otherwise, error!
a. Review subdirectories and files, especially META-INF and WEB-INF
b. Each assignment *must* have its own global subdirectory.
3. NB: Because of the way the newer version of Tomcat recognizes web applications (as well as
associated include files), *all* of the assignments will need to be placed under lis4368. 

#### [P2 README.md](p2/README.md)
1. cd to webapps subdirectory:
Example (windows): cd C:/tomcat/webapps
2. Clone assignment starter files:
git clone https://bitbucket.org/mjowett/lis4368_student_files
(clones lis4368_student_files Bitbucket repo)
Note: If there is an existing lis4368_student_files, it *will* need to be renamed—otherwise, error!
a. Review subdirectories and files, especially META-INF and WEB-INF
b. Each assignment *must* have its own global subdirectory.
3. NB: Because of the way the newer version of Tomcat recognizes web applications (as well as
associated include files), *all* of the assignments will need to be placed under lis4368. 