> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Jonathan Conley

### Assignment # Requirements:

*Sub-Heading:*

1. Install Git
2. Set up SSH for Git
3. Install JDK
4. Install Apache Tomcat

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* Git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: creates a new repository
2. git status: shows the status of directory, whether something has not been committed or the directory is clean
3. git add: adds files from workspace to index
4. git commit: commits files from index to local repository
5. git push: pushes files from local repository to remote respository
6. git pull: pulls files from remote repository to workspace
7. One additional git command: git fetch: fetches files from remote repository to local repository 

#### Assignment Screenshots:

* Link to localhost:9999
#### [localhost:9999](http://localhost:9999)

*Screenshot of Tomcat running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jtc13/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jtc13/myteamquotes "My Team Quotes Tutorial")
